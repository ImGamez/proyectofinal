package com.equipo.proyectofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import androidx.appcompat.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.equipo.proyectofinal.bluetooth.BluetoothService;
import com.equipo.proyectofinal.bluetooth.Constants;
import com.equipo.proyectofinal.bluetooth.DeviceListActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class EnviarActivity extends AppCompatActivity {
    EditText nombreEdit, apellidoEdit, direccionEdit, notasEdit;
    Button atrasBtn, enviarBtn;

    private static final String TAG = "BluetoothFragment";

    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private String mConnectedDeviceName = null;

    private BluetoothAdapter mBluetoothAdapter = null;

    private BluetoothService mService = null;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_enviar);

        nombreEdit = (EditText) findViewById(R.id.nombreEdit);
        apellidoEdit = (EditText) findViewById(R.id.apellidoEdit);
        direccionEdit = (EditText) findViewById(R.id.direccionEdit);
        notasEdit = (EditText) findViewById(R.id.notasEdit);
        atrasBtn = (Button) findViewById(R.id.atrasBtn);
        enviarBtn = (Button) findViewById(R.id.enviarBtn);

        atrasBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        enviarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombreEdit.getText().toString().length() == 0 || apellidoEdit.getText().toString().length() == 0 || direccionEdit.getText().toString().length() == 0 || notasEdit.getText().toString().length() == 0 ){
                    Toast.makeText(EnviarActivity.this, "Debe llenar todos los datos", Toast.LENGTH_SHORT).show();
                }else{
                    Persona person = new Persona(nombreEdit.getText().toString(), apellidoEdit.getText().toString(), direccionEdit.getText().toString(), notasEdit.getText().toString());
                    JSONObject jsonObject= new JSONObject();

                    try {
                        jsonObject.put("nombre", person.getNombre());
                        jsonObject.put("apellidos", person.getApellidos());
                        jsonObject.put("direccion", person.getDireccion());
                        jsonObject.put("notas", person.getNotas());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    sendMessage(jsonObject.toString());
                }
            }
        });

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBluetoothAdapter == null) {
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else if (mService == null) {
            setupChat();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            mService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mService != null) {
            if (mService.getState() == BluetoothService.STATE_NONE) {
                mService.start();
            }
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        Intent serverIntent = new Intent(EnviarActivity.this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);

        Activity activity = EnviarActivity.this;
        if (activity == null) {
            return;
        }

        mService = new BluetoothService(activity, mHandler);
    }

    private void sendMessage(String message) {
        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(EnviarActivity.this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        if (message.length() > 0) {
            byte[] send = message.getBytes();
            progress = ProgressDialog.show(EnviarActivity.this, "Progreso", "Enviando", true);
            mService.write(send);
            progress.dismiss();
            Toast.makeText(EnviarActivity.this, "Datos enviados", Toast.LENGTH_SHORT).show();
            limpiar();
        }
    }

    private void setStatus(int resId) {
        final ActionBar actionBar = getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    private void setStatus(CharSequence subTitle) {
        final ActionBar actionBar = getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Activity activity = EnviarActivity.this;
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            limpiar();
                            progress.dismiss();
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            progress = ProgressDialog.show(EnviarActivity.this, "Progreso", "Conectando...", true);
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            if(progress != null)
                                progress.dismiss();
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    String writeMessage = new String(writeBuf);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    msg(mConnectedDeviceName + ":  " + readMessage);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Conectado a "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }else{
                    msg("Cancelado");
                    finish();
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    setupChat();
                } else {
                    Log.d(TAG, "BT not enabled");
                    Activity activity = EnviarActivity.this;
                    if (activity != null) {
                        Toast.makeText(activity, R.string.bt_not_enabled_leaving,
                                Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                }
        }
    }

    void limpiar(){
        nombreEdit.setText("");
        apellidoEdit.setText("");;
        direccionEdit.setText("");
        notasEdit.setText("");
    }

    private void connectDevice(Intent data, boolean secure) {
        Bundle extras = data.getExtras();
        if (extras == null) {
            return;
        }
        String address = extras.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        mService.connect(device, secure);
    }

    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }
}
