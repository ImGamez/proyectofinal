package com.equipo.proyectofinal;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.equipo.proyectofinal.bluetooth.BluetoothService;
import com.equipo.proyectofinal.bluetooth.Constants;
import com.equipo.proyectofinal.bluetooth.DeviceListActivity;
import com.equipo.proyectofinal.firebase.ReferenciasFirebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RecibirActivity extends AppCompatActivity {
    ListView lista;
    Button volverBtn;

    private static final String TAG = "BluetoothFragment";

    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private String mConnectedDeviceName = null;

    private BluetoothAdapter mBluetoothAdapter = null;

    private BluetoothService mService = null;

    private ArrayList<Persona> listaPersonas;
    private MyArrayAdapter adapter;

    private FirebaseDatabase basedatabase;
    private DatabaseReference referencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_recibir);

        this.basedatabase = FirebaseDatabase.getInstance();
        this.referencia = this.basedatabase.getReferenceFromUrl(ReferenciasFirebase.URL_DATABASE+ ReferenciasFirebase.DATABASE_NAME + "/" + ReferenciasFirebase.TABLE_NAME);

        lista = (ListView) findViewById(R.id.lista);
        volverBtn = (Button) findViewById(R.id.volverBtn);

        volverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        listaPersonas = new ArrayList<Persona>();
        adapter = new MyArrayAdapter(this, R.layout.layout_persona, listaPersonas);
        lista.setAdapter(adapter);


        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBluetoothAdapter == null) {
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else if (mService == null) {
            setupChat();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            mService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mService != null) {
            if (mService.getState() == BluetoothService.STATE_NONE) {
                mService.start();
            }
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        Activity activity = RecibirActivity.this;
        if (activity == null) {
            return;
        }

        mService = new BluetoothService(activity, mHandler);
    }

    private void sendMessage(String message) {
        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(RecibirActivity.this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mService.write(send);
            Toast.makeText(RecibirActivity.this, "Datos enviados", Toast.LENGTH_SHORT).show();
        }
    }

    private void setStatus(int resId) {
        final ActionBar actionBar = getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    private void setStatus(CharSequence subTitle) {
        final ActionBar actionBar = getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Activity activity = RecibirActivity.this;
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    String writeMessage = new String(writeBuf);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    try {
                        JSONObject json = new JSONObject(readMessage);
                        Persona person = new Persona(json.getString("nombre"),json.getString("apellidos"),json.getString("direccion"),json.getString("notas"));
                        listaPersonas.add(person);
                        adapter = new MyArrayAdapter(RecibirActivity.this, R.layout.layout_persona, listaPersonas);
                        DatabaseReference newContactoReference = referencia.push();
                        String id = newContactoReference.getKey();
                        person.set_ID(id);
                        newContactoReference.setValue(person);
                        lista.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Conectado a "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }else{
                    msg("Cancelado");
                    finish();
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    setupChat();
                } else {
                    Log.d(TAG, "BT not enabled");
                    Activity activity = RecibirActivity.this;
                    if (activity != null) {
                        Toast.makeText(activity, R.string.bt_not_enabled_leaving,
                                Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        Bundle extras = data.getExtras();
        if (extras == null) {
            return;
        }
        String address = extras.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        mService.connect(device, secure);
    }

    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    class MyArrayAdapter extends ArrayAdapter<Persona> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Persona> personas;
        private LayoutInflater inflater;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Persona> personas) {
            super(context, textViewResourceId, personas);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.personas = personas;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombre);
            TextView lblApellido = (TextView) view.findViewById(R.id.lblApellido);
            TextView lblDireccion = (TextView) view.findViewById(R.id.lblDireccion);
            TextView lblNotas = (TextView) view.findViewById(R.id.lblNotas);


            lblNombre.setText("Nombre: " + personas.get(position).getNombre());
            lblApellido.setText("Apellidos: " + personas.get(position).getApellidos());
            lblDireccion.setText("Domicilio: " + personas.get(position).getDireccion());
            lblNotas.setText("Notas: " + personas.get(position).getNotas());

            return view;
        }
    }
}
