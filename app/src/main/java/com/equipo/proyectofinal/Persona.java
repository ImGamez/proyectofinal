package com.equipo.proyectofinal;

import java.io.Serializable;

public class Persona implements Serializable {
    private String _ID, nombre, apellidos, direccion, notas;

    public Persona(String nombre, String apellidos, String direccion, String notas) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.notas = notas;
    }

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Persona() {
    }
}
